#include "../inc/forma.hpp"
#include <bits/stdc++.h>
using namespace std;
#define LINHA 40
#define COLUNA 80
#define LIVE 'O'
#define DEAD '.'

Forma::Forma()
{
  posx = 0;
  posy = 0;
}
Forma::Forma(int posx,int posy)
{
  this->posx = posx;
  this->posy = posy;
}

int Forma::getPosx()
{
  return posx;
}
void Forma::setPosx(int posx)
{
  this->posx = posx;
}
int Forma::getPosy()
{
  return posy;
}
void Forma::setPosy(int posy)
{
  this->posy = posy;
}
