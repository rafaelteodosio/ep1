#include "../inc/glider.hpp"
#include <bits/stdc++.h>
#define LINHA 40
#define COLUNA 80
#define LIVE 'O'
#define DEAD '.'
using namespace std;

Glider::Glider()
{
  posx = 1;
  posy = 1;

  int linha[5] = {10,10,8,10,9};
  int coluna[5] = {10,11,11,12,12};

  for (int i = 0; i < 5; i++)
  {
    campo[linha[i]+posx][coluna[i]+posy] = LIVE;
  }
}

Glider::Glider(int posx,int posy)
{
  this->posx = posx;
  this->posy = posy;

  int linha[5] = {10,10,8,10,9};
  int coluna[5] = {10,11,11,12,12};

  for (int i = 0; i < 5; i++)
  {
    campo[linha[i]+posx][coluna[i]+posy] = LIVE;
  }
}
