#include "../inc/blink.hpp"
#include <bits/stdc++.h>
#define LINHA 40
#define COLUNA 80
#define LIVE 'O'
#define DEAD '.'
using namespace std;

Blink::Blink()
{
  posx = 1;
  posy = 1;

  int linha[6] = {9,8,7};
  int coluna[6] = {10,10,10};

  for (int i = 0; i < 5; i++)
  {
    campo[linha[i]+posx][coluna[i]+posy] = LIVE;
  }
}

Blink::Blink(int posx,int posy)
{
  this->posx = posx;
  this->posy = posy;

  int linha[6] = {9,8,7};
  int coluna[6] = {10,10,10};

  for (int i = 0; i < 5; i++)
  {
    campo[linha[i]+posx][coluna[i]+posy] = LIVE;
  }
}
