#include "../inc/campo.hpp"
#include <bits/stdc++.h>
#define LINHA 40
#define COLUNA 80
#define LIVE 'O'
#define DEAD '.'
using namespace std;

Campo::Campo()
{
  for (int i = 0; i < LINHA; i++)
  {
    for (int j = 0; j < COLUNA; j++)
    {
      campo[i][j] = DEAD;
    }
  }
}

int Cantos(char clone[][COLUNA],int i,int j)
{
  int vizinhos = 0;

      if (i == 0 and j == 0)
      {
        if (clone[i+1][j] == LIVE)
        {
          vizinhos++;
        }
        if (clone[i][j+1] == LIVE)
        {
          vizinhos++;
        }
        if (clone[i+1][j+1] == LIVE)
        {
          vizinhos++;
        }
      }
      if (i == LINHA-1 and j == 0)
      {
        if (clone[i-1][j] == LIVE)
        {
          vizinhos++;
        }
        if(clone[i][j+1] == LIVE)
        {
          vizinhos++;
        }
        if(clone[i-1][j+1] == LIVE)
        {
          vizinhos++;
        }
      }
      if (i == 0 and j == COLUNA-1)
      {
        if (clone[i][j-1] == LIVE)
        {
          vizinhos++;
        }
        if (clone[i+1][j] == LIVE)
        {
          vizinhos++;
        }
        if (clone[i+1][j-1] == LIVE)
        {
          vizinhos++;
        }
      }
      if (i == LINHA-1 and j == COLUNA-1)
      {
        if (clone[i-1][j-1] == LIVE)
        {
          vizinhos++;
        }
        if (clone[i-1][j] == LIVE)
        {
          vizinhos++;
        }
        if (clone[i][j-1] == LIVE)
        {
          vizinhos++;
        }
      }
  return vizinhos;
}

int Meio(char clone[][COLUNA],int i,int j)
{
  int vizinhos = 0;

        if (clone[i][j-1] == LIVE)
        {
          vizinhos++;
        }
        if (clone[i][j+1] == LIVE)
        {
          vizinhos++;
        }
        if (clone[i-1][j] == LIVE)
        {
          vizinhos++;
        }
        if (clone[i+1][j] == LIVE)
        {
          vizinhos++;
        }
        if (clone[i+1][j+1] == LIVE)
        {
          vizinhos++;
        }
        if (clone[i-1][j+1] == LIVE)
        {
          vizinhos++;
        }
        if (clone[i+1][j-1] == LIVE)
        {
          vizinhos++;
        }
        if (clone[i-1][j-1] == LIVE)
        {
          vizinhos++;
        }
      return vizinhos;
  }

int Lados(char clone[][COLUNA],int i,int j)
{
  int vizinhos = 0;
      if (i > 0 and i < LINHA-1 and j == 0)
      {
        if (clone[i-1][j] == LIVE)
        {
          vizinhos++;
        }
        if (clone[i-1][j+1] == LIVE)
        {
          vizinhos++;
        }
        if (clone[i][j+1] == LIVE)
        {
          vizinhos++;
        }
        if (clone[i+1][j] == LIVE)
        {
          vizinhos++;
        }
        if (clone[i+1][j+1] == LIVE)
        {
          vizinhos++;
        }
      }
      if (i > 0 and i < LINHA-1 and j == COLUNA-1)
      {
        if (clone[i-1][j] == LIVE)
        {
          vizinhos++;
        }
        if (clone[i-1][j-1] == LIVE)
        {
          vizinhos++;
        }
        if (clone[i][j-1] == LIVE)
        {
          vizinhos++;
        }
        if (clone[i+1][j-1] == LIVE)
        {
          vizinhos++;
        }
        if (clone[i+1][j] == LIVE)
        {
          vizinhos++;
        }
      }
      if (i == LINHA-1 and j > 0 and j < COLUNA-1)
      {
        if (clone[i][j-1] == LIVE)
        {
          vizinhos++;
        }
        if (clone[i][j+1] == LIVE)
        {
          vizinhos++;
        }
        if (clone[i-1][j-1] == LIVE)
        {
          vizinhos++;
        }
        if (clone[i-1][j+1] == LIVE)
        {
          vizinhos++;
        }
        if (clone[i-1][j] == LIVE)
        {
          vizinhos++;
        }
      }
      if (i == 0 and j > 0 and j < COLUNA-1)
      {
        if (clone[i][j-1] == LIVE)
        {
          vizinhos++;
        }
        if (clone[i+1][j-1] == LIVE)
        {
          vizinhos++;
        }
        if (clone[i+1][j] == LIVE)
        {
          vizinhos++;
        }
        if (clone[i+1][j+1] == LIVE)
        {
          vizinhos++;
        }
        if (clone[i][j+1] == LIVE)
        {
          vizinhos++;
        }
    }
  return vizinhos;
}

void Campo::aplicandoRegas()
{
  char clone[LINHA][COLUNA];
  int vizinhos = 0;
    for (int linha1 = 0; linha1 < LINHA; linha1++)
    {
      for (int coluna1 = 0; coluna1 < COLUNA; coluna1++)
      {
        clone[linha1][coluna1] = campo[linha1][coluna1];
      }
    }
    for (int i = 0; i < LINHA; i++)
    {
      for (int j = 0; j < COLUNA; j++)
        {
          if ((i == 0 and j == 0) or (i == LINHA-1 and j == COLUNA-1) or (i == 0 and j == COLUNA-1) or (i == LINHA-1 and j == 0))
          {
            vizinhos = Cantos(clone,i,j);
          } else if ((i == 0 and j > 0 and j < COLUNA-1) or (i == LINHA-1 and j > 0 and j < COLUNA-1) or (i > 0 and i < LINHA-1 and j == COLUNA-1) or (i > 0 and i < LINHA-1 and j == 0))
          {
            vizinhos = Lados(clone,i,j);
          }
          else
          {
            vizinhos = Meio(clone,i,j);
          }
          if (vizinhos > 3 or vizinhos < 2)
          {
            campo[i][j] = DEAD;
          }
            else if (vizinhos == 3 or vizinhos == 2)
          {
            if (vizinhos == 2 and clone[i][j] == DEAD)
            {
              campo[i][j] = DEAD;
            }
            else
              campo[i][j] = LIVE;
          }
        }
    }
}

void Campo::printarForma()
{
  for (int i = 0; i < LINHA/2; i++)
  {
    for ( int j = 0; j < COLUNA/2; j++)
    {
      std::cout  << campo[i][j];
    }
    std::cout << '\n';
  }
}
