#include "../inc/block.hpp"
#include <bits/stdc++.h>
#define LINHA 40
#define COLUNA 80
#define LIVE 'O'
#define DEAD '.'
using namespace std;

Block::Block()
{
  posx = 1;
  posy = 1;

  int linha[4] = {10,11,10,11};
  int coluna[4] = {10,10,11,11};

  for (int i = 0; i < 4; i++)
  {
    campo[linha[i]+posx][coluna[i]+posy] = LIVE;
  }
}

Block::Block(int posx,int posy)
{
  this->posx = posx;
  this->posy = posy;

  int linha[4] = {10,11,10,11};
  int coluna[4] = {10,10,11,11};

  for (int i = 0; i < 4; i++)
  {
    campo[linha[i]+posx][coluna[i]+posy] = LIVE;
  }
}
