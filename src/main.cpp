#include "../inc/gosperglidergun.hpp"
#include "../inc/glider.hpp"
#include "../inc/forma.hpp"
#include "../inc/block.hpp"
#include "../inc/blink.hpp"
#include <unistd.h>
#include <stdlib.h>
#include <bits/stdc++.h>
#define LINHA 40
#define COLUNA 80
#define LIVE 'O'
#define DEAD '.'
using namespace std;


int Menu()
{
  std::cout << "---------------------" << '\n' << "CONWAY's GAME OF LIFE" << '\n' << "---------------------" << '\n';
  std::cout << '\n';
  std::cout << "1 GOSPER GLIDER GUN" << '\n' << "2 BLOCK" << '\n' << "3 BLINK" << '\n' << "4 GLIDER" << '\n' << "0 SAIR" << '\n' << '\n';
  int resposta;

  std::cin >> resposta;

  switch (resposta)
  {
    case 1:
    return 1;
    break;

    case 2:
    return 2;
    break;

    case 3:
    return 3;
    break;

    case 4:
    return 4;
    break;

    default:
    return 0;
    break;
  }
}

int main(int argc, char const *argv[])
{
  int posx = 0;
  int posy = 0;
  int verifica,geracoes,cont;
  verifica = 0;
  int menu = Menu();

  if (menu == 1)
  {
    std::cout << "Digite 1 para posição padrão ou 0 para escolher uma posição" << '\n';
    std::cin >> verifica;
    if (verifica == 1)
    {
      GosperGliderGun * forma1 = new GosperGliderGun();
      std::cout << "Digite O Número De Gerações: " ;
      std::cin >> geracoes;

      for ( cont = 0; cont < geracoes; cont++)
      {
        std::cout << "Geração:" << cont+1 << '\n';
        forma1 -> aplicandoRegas();
        forma1 -> printarForma();
        usleep(90000);
        system("clear");
      }
      forma1 -> printarForma();
      delete(forma1);
    }
    else if(verifica == 0)
    {
      GosperGliderGun * forma1 = new GosperGliderGun(posx,posy);
      std::cout << "Digite a Linha e Coluna respectivamente ";
      std::cin >> posx >> posy;
      while (posx > LINHA or posy > COLUNA)
      {
        std::cout << "Digite Valores Menores Que 40 e 80" << '\n';
        std::cin >> posx >> posy;
      }
      std::cout << "Digite O Número De Gerações: " ;
      std::cin >> geracoes;
      for ( cont = 0; cont < geracoes; cont++)
      {
        std::cout << "Geração:" << cont+1 << '\n';
        forma1 -> aplicandoRegas();
        forma1 -> printarForma();
        usleep(60000);
        system("clear");
      }
      forma1 -> printarForma();
      delete(forma1);
    }
  }
  else if (menu == 2)
  {
    std::cout << "Digite 1 para posição padrão ou 0 para escolher uma posição" << '\n';
    std::cin >> verifica;
    if (verifica == 1)
    {
      Block * forma1 = new Block();
      std::cout << "Digite O Número De Gerações: " ;
      std::cin >> geracoes;
      for ( cont = 0; cont < geracoes; cont++)
      {
        std::cout << "Geração:" << cont+1 << '\n';
        forma1 -> aplicandoRegas();
        forma1 -> printarForma();
        usleep(40000);
        system("clear");
      }
      forma1 -> printarForma();
      delete(forma1);
    }
    else if(verifica == 0)
    {
      Block * forma1 = new Block(posx,posy);
      std::cout << "Digite a Linha e Coluna respectivamente ";
      std::cin >> posx >> posy;
      while (posx > LINHA or posy > COLUNA)
      {
        std::cout << "Digite Valores Menores Que 40 e 80" << '\n';
        std::cin >> posx >> posy;
      }
      std::cout << "Digite O Número De Gerações: " ;
      std::cin >> geracoes;
      for ( cont = 0; cont < geracoes; cont++)
      {
        std::cout << "Geração:" << cont+1 << '\n';
        forma1 -> aplicandoRegas();
        forma1 -> printarForma();
        usleep(40000);
        system("clear");
      }
      forma1 -> printarForma();
      delete(forma1);
    }
  }
  else if (menu == 3)
  {
    std::cout << "Digite 1 para posição padrão ou 0 para escolher uma posição" << '\n';
    std::cin >> verifica;
    if (verifica == 1)
    {
      Blink * forma1 = new Blink();
      std::cout << "Digite O Número De Gerações: " ;
      std::cin >> geracoes;
      for ( cont = 0; cont < geracoes; cont++)
      {
        std::cout << "Geração:" << cont+1 << '\n';
        forma1 -> aplicandoRegas();
        forma1 -> printarForma();
        usleep(400000);
        system("clear");
      }
      forma1 -> printarForma();
      delete(forma1);
    }
    else if(verifica == 0)
    {
      Blink * forma1 = new Blink(posx,posy);
      std::cout << "Digite a Linha e Coluna respectivamente ";
      std::cin >> posx >> posy;
      while (posx > LINHA or posy > COLUNA)
      {
        std::cout << "Digite Valores Menores Que 40 e 80" << '\n';
        std::cin >> posx >> posy;
      }
      std::cout << "Digite O Número De Gerações: " ;
      std::cin >> geracoes;
      for ( cont = 0; cont < geracoes; cont++)
      {
        std::cout << "Geração:" << cont+1 << '\n';
        forma1 -> aplicandoRegas();
        forma1 -> printarForma();
        usleep(400000);
        system("clear");
      }
      forma1 -> printarForma();
      delete(forma1);
    }
  }

  else if (menu == 4)
  {
    std::cout << "Digite 1 para posição padrão ou 0 para escolher uma posição" << '\n';
    std::cin >> verifica;
    if (verifica == 1)
    {
      Glider * forma1 = new Glider();
      std::cout << "Digite O Número De Gerações: " ;
      std::cin >> geracoes;
      for ( cont = 0; cont < geracoes; cont++)
      {
        std::cout << "Geração:" << cont+1 << '\n';
        forma1 -> aplicandoRegas();
        forma1 -> printarForma();
        usleep(400000);
        system("clear");
      }
      forma1 -> printarForma();
      delete(forma1);
    }
    else if(verifica == 0)
    {
      Glider * forma1 = new Glider(posx,posy);
      std::cout << "Digite a Linha e Coluna respectivamente ";
      std::cin >> posx >> posy;
      while (posx > LINHA or posy > COLUNA)
      {
        std::cout << "Digite Valores Menores Que 40 e 80" << '\n';
        std::cin >> posx >> posy;
      }
      std::cout << "Digite O Número De Gerações: " ;
      std::cin >> geracoes;
      for ( cont = 0; cont < geracoes; cont++)
      {
        std::cout << "Geração:" << cont+1 << '\n';
        forma1 -> aplicandoRegas();
        forma1 -> printarForma();
        usleep(400000);
        system("clear");
      }
      forma1 -> printarForma();
    }
  }
  return 0;
}
