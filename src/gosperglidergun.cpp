#include "../inc/gosperglidergun.hpp"
#include <bits/stdc++.h>
#define LINHA 40
#define COLUNA 80
#define LIVE 'O'
#define DEAD '.'
using namespace std;

GosperGliderGun::GosperGliderGun()
{
  posx = 1;
  posy = 1;

  int linha[36] = {4,5,4,5,4,5,6,3,7,2,8,2,8,5,3,7,4,5,6,5,2,3,4,2,3,4,1,5,0,1,5,6,2,3,2,3};
  int coluna[36] = {0,0,1,1,10,10,10,11,11,12,12,13,13,14,15,15,16,16,16,17,20,20,20,21,21,21,22,22,24,24,24,24,34,34,35,35};

  for (int i = 0; i < 36; i++)
  {
    campo[linha[i]+posx][coluna[i]+posy] = LIVE;
  }
}

GosperGliderGun::GosperGliderGun(int posx,int posy)
{
  this->posx = posx;
  this->posy = posy;

  int linha[36] = {4,5,4,5,4,5,6,3,7,2,8,2,8,5,3,7,4,5,6,5,2,3,4,2,3,4,1,5,0,1,5,6,2,3,2,3};
  int coluna[36] = {0,0,1,1,10,10,10,11,11,12,12,13,13,14,15,15,16,16,16,17,20,20,20,21,21,21,22,22,24,24,24,24,34,34,35,35};

  for (int i = 0; i < 36; i++)
  {
    campo[linha[i]+posx][coluna[i]+posy] = LIVE;
  }
}
