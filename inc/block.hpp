#ifndef BLOCK_HPP
#define BLOCK_HPP
#include "forma.hpp"
#include <bits/stdc++.h>
#define LINHA 40
#define COLUNA 80
#define LIVE 'O'
#define DEAD '.'
using namespace std;

class Block : public Forma
{
  public:
    Block();
    Block(int posx, int posy);
};
#endif
