#ifndef CAMPO_HPP
#define CAMPO_HPP
#include <bits/stdc++.h>
using namespace std;
#define LINHA 40
#define COLUNA 80
#define LIVE 'O'
#define DEAD '.'

class Campo
{
  protected:
    char campo[LINHA][COLUNA];
  public:
    Campo();
    void aplicandoRegas();
    void printarForma();
};
#endif
