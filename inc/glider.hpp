#ifndef GLIDER_HPP
#define GLIDER_HPP
#include "forma.hpp"
#include <bits/stdc++.h>
#define LINHA 40
#define COLUNA 80
#define LIVE 'O'
#define DEAD '.'
using namespace std;

class Glider : public Forma
{
  public:
    Glider();
    Glider(int posx, int posy);
};
#endif
