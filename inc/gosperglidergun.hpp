#ifndef GOSPERGLIDERGUN_HPP
#define GOSPERGLIDERGUN_HPP
#include "forma.hpp"
#include <bits/stdc++.h>
#define LINHA 40
#define COLUNA 80
#define LIVE 'O'
#define DEAD '.'
using namespace std;

class GosperGliderGun : public Forma
{
  public:
    GosperGliderGun();
    GosperGliderGun(int posx, int posy);
};
#endif
