#ifndef BLINK_HPP
#define BLINK_HPP
#include "forma.hpp"
#include <bits/stdc++.h>
#define LINHA 40
#define COLUNA 80
#define LIVE 'O'
#define DEAD '.'
using namespace std;

class Blink : public Forma
{
  public:
    Blink();
    Blink(int posx, int posy);
};
#endif
