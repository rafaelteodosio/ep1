#ifndef FORMA_HPP
#define FORMA_HPP
#include "campo.hpp"
#include <bits/stdc++.h>
#define LINHA 40
#define COLUNA 80
#define LIVE 'O'
#define DEAD '.'
using namespace std;

class Forma : public Campo
{
  protected:
    int posx;
    int posy;
  public:
    Forma();
    Forma(int posx,int posy);
    int getPosx();
    void setPosx(int posx);
    int getPosy();
    void setPosy(int posy);
};

#endif
